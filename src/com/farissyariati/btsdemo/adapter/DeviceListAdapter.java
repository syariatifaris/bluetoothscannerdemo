package com.farissyariati.btsdemo.adapter;

import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.content.Context;

@SuppressLint("InflateParams")
public class DeviceListAdapter extends BaseAdapter {
	private LayoutInflater mLayoutInflater;
	private List<BluetoothDevice> mData;
	private OnPairButtonClickListener mPairListener;
	private OnSendButtonClickListener mSendListener;
	
	public DeviceListAdapter(Context context) {
		this.mLayoutInflater = LayoutInflater.from(context);
	}

	/**
	 * Set Bluetooth Devices
	 * 
	 * @param devices
	 */
	public void setData(List<BluetoothDevice> devices) {
		this.mData = devices;
	}

	/**
	 * Set pairing click listener
	 * 
	 * @param listener
	 */
	public void setPairListener(OnPairButtonClickListener listener) {
		this.mPairListener = listener;
	}
	
	/**
	 * Set send click listener
	 * @param listener
	 */
	public void setSendListener(OnSendButtonClickListener listener){
		this.mSendListener = listener;
	}

	@Override
	public int getCount() {
		return (mData == null ? 0 : mData.size());
	}

	@Override
	public Object getItem(int position) {
		return this.mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mLayoutInflater.inflate(
					com.example.btsdemo.R.layout.list_item_device, null);
			holder = new ViewHolder();
			holder.tvName = (TextView) convertView
					.findViewById(com.example.btsdemo.R.id.tv_name);
			holder.tvAddress = (TextView) convertView
					.findViewById(com.example.btsdemo.R.id.tv_address);
			holder.pairButton = (Button) convertView
					.findViewById(com.example.btsdemo.R.id.btn_pair);
			holder.sendButton = (Button) convertView
					.findViewById(com.example.btsdemo.R.id.btn_send_rand_string);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		BluetoothDevice device = this.mData.get(position);
		holder.tvName.setText(device.getName());
		holder.tvAddress.setText(device.getAddress());
		holder.pairButton
				.setText((device.getBondState() == BluetoothDevice.BOND_BONDED) ? "Unpair"
						: "Pair");
		holder.pairButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mPairListener != null) {
					mPairListener.onPairButtonClick(position);
				}
			}
		});

		holder.sendButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mSendListener != null){
					mSendListener.OnSendButtonClickListenet(position);
				}
			}
		});

		return convertView;
	}

	/**
	 * 
	 * @author Faris Internal Class View Holder for Devices
	 */
	static class ViewHolder {
		TextView tvName;
		TextView tvAddress;
		Button pairButton;
		Button sendButton;
	}

	/**
	 * 
	 * @author Faris Interface On Pair Button Click Listener
	 */
	public interface OnPairButtonClickListener {
		public abstract void onPairButtonClick(int position);
	}

	public interface OnSendButtonClickListener {
		public abstract void OnSendButtonClickListenet(int position);
	}

}
