package com.example.btsdemo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;

import com.farissyariati.btsdemo.adapter.DeviceListAdapter;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.widget.ListView;
import android.widget.Toast;

/**
 * An abstract class that represents an algorithm.
 *
 * @author Faris Syariati
 *
 * @version 1.0
 */
public class DeviceListActivity extends Activity {

	/**
	 * Let's call this as the first sentence, so here, we must place a period.
	 */
	private ListView mListView;

	/**
	 * Device List Adapter This.
	 */
	private DeviceListAdapter mAdapter;

	private ArrayList<BluetoothDevice> mDeviceList;

	private BluetoothSocket btSocket = null;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_paired_device);
		this.mDeviceList = getIntent().getExtras().getParcelableArrayList(
				"device.list");
		this.mListView = (ListView) findViewById(R.id.lv_paired);
		this.mAdapter = new DeviceListAdapter(this);
		this.mAdapter.setData(mDeviceList);
		this.mAdapter
				.setPairListener(new DeviceListAdapter.OnPairButtonClickListener() {
					@Override
					public void onPairButtonClick(int position) {
						BluetoothDevice device = mDeviceList.get(position);
						if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
							unPairDevice(device);
						} else {
							showToast("Pairing..");
							pairDevice(device);
						}
					}
				});

		this.mAdapter
				.setSendListener(new DeviceListAdapter.OnSendButtonClickListener() {
					@Override
					public void OnSendButtonClickListenet(int position) {
						BluetoothDevice device = mDeviceList.get(position);
						if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
							try {
								btSocket = createBluetoothSocket(device);
							} catch (IOException ioe) {
								showToast("Socket Creation Failed");
							}
							try{
								btSocket.connect();
								SendReceiveAction srThread = new SendReceiveAction(btSocket);
								srThread.start();
								srThread.write("This is String From BlueTooth");
								showToast("Message Send");
								btSocket.close();
							}catch(IOException e){
								showToast("Socket Connection Failed");
								try{
									btSocket.close();
								}catch(IOException ioe){
									showToast("Socket Close Failed Too");
								}
							}
						}else{
							showToast("Device is not pairing");
						}
					}
				});
		mListView.setAdapter(mAdapter);
		registerReceiver(mPairReceiver, new IntentFilter(
				BluetoothDevice.ACTION_BOND_STATE_CHANGED));
	}

	/**
	 * Pair the devices
	 * 
	 * @param device
	 */
	private void pairDevice(BluetoothDevice device) {
		try {
			Method method = device.getClass().getMethod("createBond",
					(Class[]) null);
			method.invoke(device, (Object[]) null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Unpair the devices
	 * 
	 * @param device
	 */
	private void unPairDevice(BluetoothDevice device) {
		try {
			Method method = device.getClass().getMethod("removeBond",
					(Class[]) null);
			method.invoke(device, (Object[]) null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create short toast
	 * 
	 * @param message
	 */
	private void showToast(String message) {
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
				.show();
	}

	private BluetoothSocket createBluetoothSocket(BluetoothDevice device)
			throws IOException {
		ParcelUuid[] uuids = device.getUuids();
		return device.createInsecureRfcommSocketToServiceRecord(uuids[0].getUuid());
		//return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
		// creates secure outgoing connecetion with BT device using UUID
	}

	private final BroadcastReceiver mPairReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
				final int state = intent
						.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE,
								BluetoothDevice.ERROR);
				final int prevState = intent.getIntExtra(
						BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE,
						BluetoothDevice.ERROR);

				if (state == BluetoothDevice.BOND_BONDED
						&& prevState == BluetoothDevice.BOND_BONDING) {
					showToast("Paired");
				} else if (state == BluetoothDevice.BOND_NONE
						&& prevState == BluetoothDevice.BOND_BONDED) {
					showToast("Unpaired");
				}
				mAdapter.notifyDataSetChanged();
			}
		}
	};

	class SendReceiveAction extends Thread {

		private InputStream mInputStream;
		private OutputStream mOutputStream;

		public SendReceiveAction(BluetoothSocket socket) {
			this.mInputStream = null;
			this.mOutputStream = null;

			try {
				this.mInputStream = socket.getInputStream();
				this.mOutputStream = socket.getOutputStream();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			byte[] buffer = new byte[256];
			int bytes;
			while (true) {
				try {

				} catch (Exception ioe) {
					ioe.printStackTrace();
					break;
				}
			}
		}

		/**
		 * Write a string
		 * 
		 * @param input
		 */
		public void write(String input) {
			byte[] msgBuffer = input.getBytes();
			try {
				this.mOutputStream.write(msgBuffer);
			} catch (IOException e) {
				Toast.makeText(getBaseContext(), "Connection Failure",
						Toast.LENGTH_LONG).show();
				finish();
			}
		}
	}
}
