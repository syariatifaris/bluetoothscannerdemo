package com.example.btsdemo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class TestActivity extends Activity {
	
	private TextView mTestTextView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mTestTextView = (TextView)findViewById(R.id.tv_mytext);
		setContentView(R.layout.test_activity);
//		TextView myTv = new TextView(this);
//		myTv.setText("Test Activity");
//		setContentView(myTv);
	}
}
