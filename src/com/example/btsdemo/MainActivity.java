package com.example.btsdemo;

import java.util.ArrayList;
import java.util.Set;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private TextView mStatusTv;
	private Button mActivateButton;
	private Button mPairedButton;
	private Button mScanButton;

	private BluetoothAdapter mBluetoothAdapter;
	private ProgressDialog mProgressDialog;
	private ArrayList<BluetoothDevice> mDeviceList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.initializeUI();
		this.initializeBluetoothDevices();
		this.initializeBluetoothAdapter();

		// initialize progress bar
		this.mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Scanning");
		mProgressDialog.setCancelable(true);
		mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						mBluetoothAdapter.cancelDiscovery();
					}
				});

		if (this.mBluetoothAdapter == null) {
			this.showUnsupported();
		} else {
			
			//Button paired click listener
			this.mPairedButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
							.getBondedDevices();
					if (pairedDevices == null || pairedDevices.size() == 0) {
						showToast("No Paired Device Found");
					} else {
						ArrayList<BluetoothDevice> devices = new ArrayList<BluetoothDevice>();
						devices.addAll(pairedDevices);
						//change activity not implemented yet
						Intent intent = new Intent(MainActivity.this, DeviceListActivity.class);
						intent.putParcelableArrayListExtra("device.list", devices);
						startActivity(intent);
					}
				}
			});
			
			//button scan listener
			this.mScanButton.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					mBluetoothAdapter.startDiscovery();					
				}
			});
			
			//button activate listener
			this.mActivateButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(mBluetoothAdapter.isEnabled()){
						mBluetoothAdapter.disable();
						showDisabled();
					}else{
						Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
						startActivityForResult(intent, 1000);
					}	
				}
			});
			
			if(this.mBluetoothAdapter.isEnabled()){
				showEnabled();
			}else{
				showDisabled();
			}
		}
		
		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
		filter.addAction(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		registerReceiver(this.mReceiver, filter);
	}

	/**
	 * Initialize User Interface
	 */
	private void initializeUI() {
		this.mStatusTv = (TextView) findViewById(R.id.tv_status);
		this.mActivateButton = (Button) findViewById(R.id.btn_enable);
		this.mPairedButton = (Button) findViewById(R.id.btn_view_paired);
		this.mScanButton = (Button) findViewById(R.id.btn_scan);
	}

	/**
	 * Initialize device List
	 */
	private void initializeBluetoothDevices() {
		this.mDeviceList = new ArrayList<BluetoothDevice>();
	}

	/**
	 * Initialize Bluetooth Adapter
	 */
	private void initializeBluetoothAdapter() {
		this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	}

	/**
	 * Show that device does not support bluetooth
	 */
	private void showUnsupported() {
		this.mStatusTv.setText("Bluetooth is not supported on this device");
		this.mActivateButton.setEnabled(true);
		this.mPairedButton.setEnabled(false);
		this.mScanButton.setEnabled(false);
	}

	/**
	 * Create a toast
	 */
	private void showToast(String text) {
		Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT)
				.show();
	}
	
	/**
	 * Bluetooth active state
	 */
	private void showEnabled(){
		this.mStatusTv.setText("Bluetooth is On");
		this.mStatusTv.setTextColor(Color.BLUE);
		
		this.mActivateButton.setText("Disable");
		this.mActivateButton.setEnabled(true);
		
		this.mPairedButton.setEnabled(true);
		this.mScanButton.setEnabled(true);	
	}
	
	/**
	 * Bluetooth off state
	 */
	private void showDisabled(){
		this.mStatusTv.setText("Bluetooth is Off");
		this.mStatusTv.setTextColor(Color.RED);
		
		this.mActivateButton.setText("Enabled");
		this.mActivateButton.setEnabled(true);
		
		this.mPairedButton.setEnabled(false);
		this.mScanButton.setEnabled(false);
	}
	
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if(BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)){
				final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
				if(state == BluetoothAdapter.STATE_ON){
					showToast("Enabled");
					showEnabled();
				}
			}else if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)){
				mDeviceList = new ArrayList<BluetoothDevice>();
				mProgressDialog.show();
			}else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
				mProgressDialog.dismiss();
				Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
				newIntent.putParcelableArrayListExtra("device.list", mDeviceList);
				startActivity(newIntent);
			}else if(BluetoothDevice.ACTION_FOUND.equals(action)){
				BluetoothDevice device = (BluetoothDevice)intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				mDeviceList.add(device);
				showToast("Found Device: "+device.getName());
			}
		}
	};
	
	
}
